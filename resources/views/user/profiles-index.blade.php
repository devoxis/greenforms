@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">{{__('Profiles')}}
                      <span class="pull-right">
                        <a href="/profiles/create">
                          <button class="btn btn-sm btn-dark">
                              Create a new Profile
                          </button>
                        </a>
                      </span>
                    </div>
                    <div class="text-center">
                      <p class="my-5">
                        <strong>Profiles </strong> help you complete forms much faster, and they are easy and free to setup. <br/><br/>
                        All information is encrypted and government by our <a href="/privacy-policy">Privacy Policy</a><br/><br/>
                        To get started click the button below to add your information. <br/><br/>
                        <a href="/profiles/create">
                          <button class="btn btn-sm btn-primary">
                              Create a new Profile
                          </button>
                        </a>
                      </p>
                    </div>
                    <div class="container">

                    <div class="row">
                      <div class="col-md-3">
                        <div class="card-body mt-5">
                          <nav class="nav flex-column">
                            <a class="nav-link disabled"><strong>Main</strong> <i class="fa fa-star-o text-dark"></i></a>
                            <a class="nav-link text-dark" href="#"><strong>Other</strong></a>
                            <a class="nav-link text-dark" href="#"><strong>Not serious</strong></a>
                          </nav>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="card-body">
                          <nav>
                            <div class="nav nav-dark nav-tabs" id="nav-tab" role="tablist">
                              <a class="nav-item nav-link text-dark active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Personal</a>
                              <a class="nav-item nav-link text-dark" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Address</a>
                              <a class="nav-item nav-link text-dark" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Dependants</a>
                            </div>
                          </nav>
                          <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">...</div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</home>
@endsection
