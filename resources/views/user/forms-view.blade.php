@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">

      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header bg-light">{{__('RA-FRM-34568 : Vehicle Renewal Form')}}
              <div class="pull-right">
                <a href="#" class="text-success"><i class="fa fa-download"></i></a>
                <a href="#" class="text-secondary ml-3"><i class="fa fa-print"></i></a>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="card-body bg-white">
                  <div class="form-group row mb-1">
                    <label class="col-md-12 font-weight-light">
                      This is the description of the form. Hello I am the form, I do a lot of things, just ocmplete me.
                    </label>
                  </div>
                  <div class="form-group row mb-4 bg-white p-2 m-0 shadow-sm">
                    <label class="col-md-12 font-weight-light">
                      <small>Instructions: These are the instructiolns, slightly smaller font than the form description.</small>
                    </label>
                  </div>

                  <div class="form-row">
                    <div class="col-md-12 pb-2 mb-2">
                      PERSONAL INFORMATION
                    </div>
                  </div>
                  <div class="form-row pb-2">
                    <div class="col-md-4">
                      <label class="font-weight-bold p-0 m-0"><small><strong>Title</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0" id="validationDefault02" required>
                    </div>
                    <div class="col-md-4">
                      <label for="validationDefault02" class="font-weight-bold border-0 p-0 m-0"><small><strong>Initials</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0" id="validationDefault02" required>
                    </div>
                    <div class="col-md-4">
                      <label for="validationDefault02" class="font-weight-bold border-0 p-0 m-0"><small><strong>Surname</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0 " id="validationDefault02" required>
                    </div>
                  </div>
                  <div class="form-row pb-2">
                    <div class="col-md-4">
                      <label class="font-weight-bold p-0 m-0"><small><strong>Title</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0" id="validationDefault02" required>
                    </div>
                    <div class="col-md-4">
                      <label for="validationDefault02" class="font-weight-bold border-0 p-0 m-0"><small><strong>Initials</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0" id="validationDefault02" required>
                    </div>
                    <div class="col-md-4">
                      <label for="validationDefault02" class="font-weight-bold border-0 p-0 m-0"><small><strong>Surname</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0 " id="validationDefault02" required>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-12 pb-2 mb-2 mt-3">
                      ADDRESS INFORMATION
                    </div>
                  </div>
                  <div class="form-row pb-2">
                    <div class="col-md-4">
                      <label class="font-weight-bold p-0 m-0"><small><strong>Title</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0" id="validationDefault02" required>
                    </div>
                    <div class="col-md-4">
                      <label for="validationDefault02" class="font-weight-bold border-0 p-0 m-0"><small><strong>Initials</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0" id="validationDefault02" required>
                    </div>
                    <div class="col-md-4">
                      <label for="validationDefault02" class="font-weight-bold border-0 p-0 m-0"><small><strong>Surname</strong></small></label>
                      <input type="text" readonly class="form-control-plaintext border-bottom p-0 m-0 " id="validationDefault02" required>
                    </div>
                  </div>
                  <div class="form-group row mb-4 bg-white p-2 mt-4 m-0 shadow-sm">
                    <label class="col-md-12 font-weight-light">
                      <small>Footer Instructions: These are the instructiolns, slightly smaller font than the form description.</small>
                    </label>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </home>
@endsection
