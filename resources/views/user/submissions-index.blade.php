@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">{{__('Submissions')}}
                      <span class="pull-right hide">
                        <a href="/admin/fields/create">
                          <button class="btn btn-sm btn-dark">
                              Add a Field
                          </button>
                        </a>
                      </span>
                    </div>

                    <div class="table-responsive table-hover">
                      <table class="table table-valign-middle mb-0">

                        <thead>
                          <tr>
                            <th class="th-fit"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Field type</th>
                            <th scope="col">Data</th>
                            <th>&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th>1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>Otto</td>
                            <td class="td-fit">
                                <a href="/submissions/view">
                                  <button class="btn btn-sm btn-outline-primary">
                                      <i class="fa fa-cog"></i>
                                  </button>
                                </a>
                                <a href="#">
                                  <button class="btn btn-sm btn-outline-danger">
                                      <i class="fa fa-times"></i>
                                  </button>
                                </a>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>
@endsection
