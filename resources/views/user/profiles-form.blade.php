@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              Create a Profile
            </div>
            <div class="container">

              <div class="row">
                <div class="card-body">
                  <div id="form-vertical">
                    <h3>Personal Information</h3>
                    <section>
                      <form>
                        <div class="form-row">
                          <div class="col-md-4 mb-3">
                            <label for="validationDefault01">Title</label>
                            <select class="form-control custom-select">
                              <option selected>Choose...</option>
                              <option>Mr</option>
                              <option>Mrs</option>
                              <option>Ms</option>
                            </select>
                          </div>
                          <div class="col-md-4 mb-3">
                            <label for="validationDefault02">Initials</label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Last name" value="Otto" required>
                          </div>
                          <div class="col-md-4 mb-3">
                            <label for="validationDefault02" class="font-weight-light">Surname</label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Last name" value="Otto" required>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="col-md-12 mb-3">
                            <label for="validationDefault03">Full name</label>
                            <input type="text" class="form-control" id="validationDefault03" placeholder="City" required>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault04">First name</label>
                            <input type="text" class="form-control" id="validationDefault04" placeholder="State" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault05">Last name</label>
                            <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required>
                          </div>
                        </div>
                        <div class="form-row border-bottom mb-3">
                          <legend class="col-form-label col-sm-12 pt-0">Field Name <small><a href="#" >[+] Add row</a>&nbsp;<a href="#" class="text-danger">[-] Remove row</a></small>  </legend>
                          <div class="col-md-2 mb-3">
                            <label for="validationDefault04">First</label>
                            <input type="text" class="form-control" id="validationDefault04" placeholder="State" required>
                          </div>
                          <div class="col-md-2 mb-3">
                            <label for="validationDefault05">Second</label>
                            <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required>
                          </div>
                          <div class="col-md-2 mb-3">
                            <label for="validationDefault05">Second</label>
                            <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required>
                          </div>
                          <div class="col-md-2 mb-3">
                            <label for="validationDefault05">Second</label>
                            <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required>
                          </div>
                          <div class="col-md-2 mb-3">
                            <label for="validationDefault05">Second</label>
                            <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required>
                          </div>
                          <div class="col-md-2 mb-3">
                            <label for="validationDefault05">Second</label>
                            <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="col-md-12 mb-3">
                            <label for="validationDefault03">Textarea</label>
                            <textarea class="form-control" name="name" rows="5"></textarea>
                          </div>
                        </div>
                        <div class="form-row">

                          <div class="col-md-4 mb-3">
                            <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Title</small></label>
                            <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Mr" required>
                          </div>
                          <div class="col-md-4 mb-3">
                            <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Initials</small></label>
                            <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="TH" required>
                          </div>
                          <div class="col-md-4 mb-3">
                            <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Surname</small></label>
                            <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Hamata" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                            <label class="form-check-label" for="invalidCheck2">
                              Agree to terms and conditions
                            </label>
                          </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Submit form</button>
                      </form>
                    </section>
                    <h3>Address</h3>
                    <section>
                      <p>Wonderful transition effects.</p>
                    </section>
                    <h3>Dependants</h3>
                    <section>
                      <p>The next and previous buttons help you to navigate through your content.</p>
                    </section>
                    <h3>Vehicles</h3>
                    <section>
                      <p>The next and previous buttons help you to navigate through your content.</p>
                    </section>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </home>
@endsection
