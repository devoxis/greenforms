@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">

        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                  <div class="ribbon ribbon-top-right"><span>Pending</span></div>
                    <div class="card-header">{{__('Submission: RA-08999')}}</div>
                    <div class="container">
                      <div class="row">
                        <div class="card-body">
                          <div class="form-group row mb-1">
                            <label class="col-md-4 font-weight-light"><small>Form</small></label>

                            <div class="col-md-6">
                              <small>Vehicle Renewal Form</small>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-md-4 font-weight-light"><small>Date Submitted</small></label>

                            <div class="col-md-6">
                              <small>13 September 2020 at 12:09</small>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="col-md-12 pb-2 mb-2 border-bottom">
                            PERSONAL INFORMATION
                            </div>
                          </div>
                          <div class="form-row pb-2">
                            <div class="col-md-4">
                              <label class="font-weight-light border-0 p-0 m-0"><small>Title</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Mr" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Initials</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="TH" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Surname</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Hamata" required>
                            </div>
                          </div>
                          <div class="form-row pb-2">
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Title</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Mr" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Initials</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="TH" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Surname</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Hamata" required>
                            </div>
                          </div>
                          <div class="form-row mb-2">
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Title</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Mr" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Initials</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="TH" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Surname</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Hamata" required>
                            </div>
                          </div>
                          <div class="form-row mb-4">
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Title</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Mr" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Initials</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="TH" required>
                            </div>
                            <div class="col-md-4">
                              <label for="validationDefault02" class="font-weight-light border-0 p-0 m-0"><small>Surname</small></label>
                              <input type="text" readonly class="form-control-plaintext border-0 p-0 m-0 font-weight-bold" id="validationDefault02" placeholder="Last name" value="Hamata" required>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="col-md-12 pb-2 mb-2 border-bottom">
                            DOCUMENT HISTORY
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="col-md-12 mb-3 text-smaller">
                              <div class="d-flex mb-0 bg-white">
                                <div class="p-2">Captured</div>
                                <div class="p-2">By Tuyoleni Hamata</div>
                                <div class="p-2"><em>13 September 2020 at 12:09 (3 hours ago)</em></div>
                              </div>
                              <div class="d-flex">
                                <div class="p-2">Approved</div>
                                <div class="p-2">By John Smith</div>
                                <div class="p-2"><em>13 September 2020 at 13:09 (2 hours ago)</em></div>
                              </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="col-md-12 pb-2 mb-3 border-bottom">
                            ACTION
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="col-md-12">
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Receive</button>
                              <button type="button" class="btn btn-success">Approve</button>
                              <button type="button" class="btn btn-warning">Send Back</button>
                              <button type="button" class="btn btn-danger">Reject</button>
                              <button type="button" class="btn btn-outline-secondary"><i class="fa fa-money"></i> Payment</button>
                              <button type="button" class="btn btn-outline-secondary"><i class="fa fa-file-o"></i> Collection</button>
                              <button type="button" class="btn btn-outline-secondary"><i class="fa fa-truck"></i> Delivery</button>
                            </div>
                          </div>

                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Confirm Receipt</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                ...
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Confirm</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</home>
@endsection
