<!-- Left Side Of Navbar -->
  <li class="nav-item ml-2">
    <a class="nav-link" href="/home">Dashboard</a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Basket
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <h6 class="dropdown-header">{{__('Submissions')}}</h6>
      <a class="dropdown-item" href="/submissions/">Pending <span class="badge badge-pill badge-primary">8</span></a>
      <a class="dropdown-item" href="/submissions/">My Submissions</a>
      <a class="dropdown-item" href="/submissions/">All Submissions</a>
      <div class="dropdown-divider"></div>
      <h6 class="dropdown-header">{{__('Workflow')}}</h6>
      <a class="dropdown-item" href="/workflow/">Pending <span class="badge badge-pill badge-primary">8</span> </a>
      <a class="dropdown-item" href="/workflow/">All forms</a>
    </div>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Deliveries
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="/deliveries/">Pending <span class="badge badge-pill badge-primary">8</span></a>
      <a class="dropdown-item" href="/deliveries/">All Deliveries</a>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/profiles">Profiles</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/forms">Forms</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/submissions">Submissions</a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Admin
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <h6 class="dropdown-header">{{__('Forms')}}</h6>
      <a class="dropdown-item" href="/admin/forms">View Forms</a>
      <h6 class="dropdown-header">{{__('Fields')}}</h6>
      <a class="dropdown-item" href="/admin/fields">View Fields</a>
      <div class="dropdown-divider"></div>
      <h6 class="dropdown-header">{{__('Form Settings')}}</h6>
      <a class="dropdown-item" href="/admin/sections">Sections</a>
      <a class="dropdown-item" href="/admin/fieldtypes">Field Types</a>
    </div>
  </li>
