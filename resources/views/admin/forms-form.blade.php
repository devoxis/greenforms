@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              Add a Form
            </div>

            <div class="card-body">
              <!-- Success Message -->
              <div class="alert alert-success">
                Form Added successfully!!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form role="form" id="form-saving" method="POST" action="/admin/forms">
                @csrf
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Team')}}</label>

                  <div class="col-md-6">
                    <select class="form-control custom-select" name="team_id">
                      <option selected>Choose a Team</option>
                      @foreach($teams as $value => $key)
                        <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach
                    </select>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>

                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Category')}}</label>

                  <div class="col-md-6">
                    <select id="category" class="form-control custom-select" name="category_id">
                      <option selected>Choose a form category</option>
                      @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->team->name }} / {{ $category->name }}</option>
                      @endforeach
                      <option value="0">Create a new Category...</option>
                    </select>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>

                  <div id="NewCategory" class="offset-md-4 col-md-6 mt-3" style="display:none">
                    <input type="text" class="form-control" name="category_name" placeholder="Name of new category" id="new_category_id">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <!-- Name -->
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="name">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Description')}}</label>

                  <div class="col-md-6">
                    <textarea class="form-control" name="description" rows="2"></textarea>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Header')}}</label>

                  <div class="col-md-6">
                    <textarea class="form-control" name="header" rows="2"></textarea>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Footer')}}</label>

                  <div class="col-md-6">
                    <textarea class="form-control" name="footer" rows="2"></textarea>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <fieldset class="form-group">
                  <div class="row">
                    <legend class="col-form-label col-md-4 text-md-right pt-0">Delivery</legend>
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck1" value="1" name="has_delivery">
                      </div>
                    </div>
                  </div>
                </fieldset>
                <div>
                	<legend class="card-header col-form-label text-md-center col-md-12 mb-3 bg-white"><strong>Form Structure</strong></legend>
                </div>
                <div class="after-add-sections-more">
                  <div class="jumbotron jumbotron-fluid p-2 section-div" data-section="1">
                    <div class="container">
                      <div class="form-row">
                        <div class="form-group input-group col-md-4">
                          <select class="form-control custom-select section-box" name="sections[1]" id="1">
                            <option selected>Choose a Section</option>
                            @foreach($sections as $value => $key)
                              <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                          </select>
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary add-sections-more" type="button"><i class="fa fa-plus"></i></button>
                          </div>
                        </div>
                      </div>
                      <hr class="py-1" />
                      <div class="after-add-fields-more">
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <select id="inputState" class="form-control custom-select" name="fields1[1][1]">
                              <option selected>Choose...</option>
                              @foreach($fields as $value => $key)
                                <option value="{{ $key }}">{{ $value }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <select id="inputState" class="form-control custom-select" name="fields2[1][1]">
                              <option selected>Choose...</option>
                              @foreach($fields as $value => $key)
                                <option value="{{ $key }}">{{ $value }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-4 input-group">
                            <select id="inputState" class="form-control custom-select" name="fields3[1][1]">
                              <option selected>Choose...</option>
                              @foreach($fields as $value => $key)
                                <option value="{{ $key }}">{{ $value }}</option>
                              @endforeach
                            </select>
                            <div class="input-group-append">
                              <button class="btn btn-outline-secondary add-fields" type="button"><i class="fa fa-plus"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Save Button -->
                <div class="form-group row mb-0">
                  <div class="offset-md-10 col-md-2">
                    <button type="submit" class="btn btn-block btn-primary" id="submit-btn">
                      Continue >>
                    </button>
                  </div>
                </div>
              </form>

              <div class="copy-section hide">
              <div class="jumbotron jumbotron-fluid p-2 section-row" data-section="">
                <div class="container">
                  <div class="form-row">
                    <div class="form-group input-group col-md-4">
                      <select class="form-control custom-select section-box" name="sections[]" id="">
                        <option selected>Choose a Section</option>
                        @foreach($sections as $value => $key)
                          <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                      </select>
                      <div class="input-group-append">
                        <button class="btn btn-warning remove-section" type="button"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>
                  </div>
                  <hr class="py-1" />
                  <div class="after-add-fields-more">
                    <input type="hidden" class="store-row-count" value="0" >
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <select id="inputState" class="form-control custom-select field1" name="">
                          <option selected>Choose...</option>
                          @foreach($fields as $value => $key)
                            <option value="{{ $key }}">{{ $value }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group col-md-4">
                        <select id="inputState" class="form-control custom-select field2" name="">
                          <option selected>Choose...</option>
                          @foreach($fields as $value => $key)
                            <option value="{{ $key }}">{{ $value }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group col-md-4 input-group">
                        <select id="inputState" class="form-control custom-select field3" name="">
                          <option selected>Choose...</option>
                          @foreach($fields as $value => $key)
                            <option value="{{ $key }}">{{ $value }}</option>
                          @endforeach
                        </select>
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary add-fields" type="button"><i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>


              <div class="copy-fields hide">
              <div class="form-row fields-row">
                <div class="form-group col-md-4">
                  <select id="inputState" class="form-control custom-select field1" name="">
                    <option selected>Choose...</option>
                    @foreach($fields as $value => $key)
                      <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <select id="inputState" class="form-control custom-select field2" name="">
                    <option selected>Choose...</option>
                    @foreach($fields as $value => $key)
                      <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-4 input-group">
                  <select id="inputState" class="form-control custom-select field3" name="">
                    <option selected>Choose...</option>
                    @foreach($fields as $value => $key)
                      <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-warning remove-fields" type="button"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
              </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  </home>
@endsection
