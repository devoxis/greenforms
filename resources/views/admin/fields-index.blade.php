@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">{{__('Fields')}}
              <span class="pull-right">
                <a href="/admin/fields/create">
                  <button class="btn btn-sm btn-dark">
                    Add a Field
                  </button>
                </a>
              </span>
            </div>

            <div class="table-responsive table-hover">
              <table class="table table-valign-middle mb-0">

                <thead>
                  <tr>
                    <th class="th-fit"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Section</th>
                    <th scope="col">Field type</th>
                    <th scope="col">Data</th>
                    <th scope="col">Profile</th>
                    <th scope="col">Required</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($response as $item)
                    <tr>
                      <th></th>
                      <td>{{ $item->name }}<br> <small>{{ $item->description }}</small> </td>
                      <td>{{ $item->section->name }}</td>
                      <td>{{ $item->fieldtype->name }}</td>
                      <td>
                        @isset($item->options)
                          <small><strong>Options:</strong><br>
                          @foreach($item->options->data as $data)
                              {{ $data }}
                              @if(!$loop->last)
                              |
                              @endif
                          @endforeach
                          </small>
                        @endisset
                        @isset($item->structure)
                          <small><strong>Fields:</strong><br>
                          @foreach($item->structure->data as $data)
                              {{ \App\Field::find($data)->name }}
                              @if(!$loop->last)
                              |
                              @endif
                          @endforeach
                          </small>
                        @endisset

                    </td>
                    <td>
                      @if($item->is_profile)
                        <span class="badge badge-success"><i class="fa fa-check"></i></span>
                      @endif
                    </td>
                    <td>
                      @if($item->is_required)
                        <span class="badge badge-success"><i class="fa fa-check"></i></span>
                      @endif
                    </td>
                    <td class="td-fit">
                      <form action="{{ url('/admin/fields', ['id' => $item->id]) }}" method="post">
                        <a href="#">
                          <button class="btn btn-sm btn-outline-primary">
                            <i class="fa fa-cog"></i>
                          </button>
                        </a>
                        @method('delete')
                        @csrf
                        <button class="btn btn-sm btn-outline-danger" type="submit" id="deleteButton">
                          <i class="fa fa-times"></i>
                        </button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</home>
@endsection
