@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              View Profile Section
            </div>

            <div class="card-body">
              <!-- Success Message -->
              <div class="alert alert-success">
                Section Added successfully!!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form role="form" class="sort-form">
                <!-- Name -->
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Section Name')}}</label>

                  <div class="col-md-6">
                    <label class="col-form-label text-md-right"><small>Personal Information</small></label>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Description')}}</label>

                  <div class="col-md-6">
                    <label class="col-form-label text-md-right"><small>This section contains personal information about the user.</small></label>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="row">
                  <legend class="card-header col-form-label text-md-center col-md-12 mb-3 bg-white"><strong>Please reorder the fields below</strong></legend>
                  <div class="col-md-4">&nbsp;</div>
                  <div class="col-md-6">
                    <input type="hidden" name="key" value="">
                    <input type="hidden" name="uuid" value="">
                        <input type='text' name='thedata' id='thedata'>
                    <div id="ballot" class="center">
                    <ol id="sortable" class="rankings sort-row">
                      <li id='ranking_1' class="ranking">Jamie</li>
                      <li id='ranking_2' class="ranking">Joanie</li>
                      <li id='ranking_3' class="ranking">Morley</li>
                      <li id='ranking_4' id='ranking_1' class="ranking">Frank</li>
                      <li id='ranking_5' class="ranking">Larry</li>
                    </ol>
                    </div>
                  </div>
                </div>
                <!-- Save Button -->
                <div class="form-group row mb-0">
                  <div class="offset-md-4 col-md-6">
                    <button type="submit" class="btn btn-primary">
                      Save
                    </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</home>
@endsection
