@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              Define Form Workflow
            </div>

            <div class="card-body">
              <!-- Success Message -->
              <div class="alert alert-success">
                Form Added successfully!!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form role="form" id="form-workflow" method="POST" action="/admin/forms/workflow">
                @csrf
                <input type="hidden" class="form-control" name="form_id" value="{{$form->id}}">
                <!-- Name -->
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Form Name')}}</label>

                  <div class="col-md-6">
                    <label class="col-form-label text-md-right"><small>{{$form->name}}</small></label>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Description')}}</label>

                  <div class="col-md-6">
                    <label class="col-form-label text-md-right"><small>{{$form->description}}</small></label>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>

                <div>
                  <legend class="card-header col-form-label text-md-center col-md-12 mb-3 bg-white"><strong>Please add the workflow stages</strong></legend>
                  <div class="row mb-2">
                    <div class="col-md-4"><small>Workflow Stage</small></div>
                    <div class="col-md-4"><small>Users</small></div>
                    <div class="col-md-1 text-center"><small><i class="fa fa-envelope"></i><br>Rejected</small></div>
                    <div class="col-md-1 text-center"><small><i class="fa fa-envelope"></i><br>Approved</small></div>
                    <div class="col-md-1 text-center"><small><i class="fa fa-dollar"></i><br>Payment</small></div>
                    <div class="col-md-1">&nbsp;</div>
                  </div>
                  <hr>
                  <div class="form-group row after-add-more-stages mb-3">
                    <div class="col-md-4">
                      <input type="text" class="form-control name1" name="name[1]">

                      <span class="invalid-feedback">
                        Error
                      </span>
                    </div>
                    <div class="col-md-4">
                      <select class="form-control custom-select select1" name="users[1][]" multiple>
                        <option selected value="0">Choose...</option>
                        @foreach($users as $user)
                          <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                      </select>

                      <span class="invalid-feedback">
                        Error
                      </span>
                    </div>
                    <div class="col-md-1 text-center">
                      <div class="form-check">
                        <input class="form-check-input check1"  name="is_notify_approved[1]" type="checkbox" value="1" checked>
                      </div>
                    </div>
                    <div class="col-md-1 text-center">
                      <div class="form-check">
                        <input class="form-check-input check2" name="is_notify_declined[1]" type="checkbox" value="1" checked>
                      </div>
                    </div>
                    <div class="col-md-1 text-center">
                      <div class="form-check">
                        <input class="form-check-input check3" name="is_payment[1]" type="checkbox" value="1">
                      </div>
                    </div>
                    <div class="col-md-1">
                      <div class="input-group-append change">
                        <label for="">&nbsp;</label><br/>
                        <a class="btn btn-outline btn-dark add-more-stages"><i class="fa fa-plus"></i></a>
                      </div>
                    </div>
                  </div>
                </div>



                <!-- Save Button -->
                <div class="form-group row mb-0">
                  <div class="offset-md-4 col-md-6">
                    <button type="submit" class="btn btn-primary">
                      Save
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </home>
@endsection
