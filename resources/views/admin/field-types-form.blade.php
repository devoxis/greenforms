@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              Add a Field Type
            </div>

            <div class="card-body">
              <!-- Success Message -->
              <div class="alert alert-success">
                Field Type Added successfully!!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form role="form" method="POST" action="/admin/fieldtypes">
                <!-- Name -->
                @csrf
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="name">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Columns')}}</label>

                  <div class="col-md-2">
                    <select class="form-control custom-select" name="data[][cols]">
                      <option value="1" selected>1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                    </select>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <fieldset class="form-group">
                  <div class="row">
                    <legend class="col-form-label col-md-4 text-md-right pt-0">Features</legend>
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="data[][features]" id="gridRadios1" value="options">
                        <label class="form-check-label" for="gridRadios1">
                          Has Options
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="data[][features]" id="gridRadios2" value="structure">
                        <label class="form-check-label" for="gridRadios2">
                          Has Structure
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="data[][features]" id="gridRadios3" value="none" checked>
                        <label class="form-check-label" for="gridRadios3">
                          None
                        </label>
                      </div>
                    </div>
                  </div>
                </fieldset>

                <!-- Save Button -->
                <div class="form-group row mb-0">
                  <div class="offset-md-4 col-md-6">
                    <button type="submit" class="btn btn-primary">
                      Save
                    </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</home>
@endsection
