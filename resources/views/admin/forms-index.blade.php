@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">{{__('Forms')}}
                      <span class="pull-right">
                        <a href="/admin/forms/create">
                          <button class="btn btn-sm btn-dark">
                              Add a Form
                          </button>
                        </a>
                      </span>
                    </div>

                    <div class="table-responsive table-hover">
                      <table class="table table-valign-middle mb-0">

                        <thead>
                          <tr>
                            <th class="th-fit"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Team</th>
                            <th scope="col">Delivery</th>
                            <th scope="col">Published</th>
                            <th>&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($forms as $item)
                            <tr>
                              <th></th>
                              <td>{{ $item->name }}</td>
                              <td>{{ $item->team->name }}</td>
                              <td>
                                @if($item->has_delivery)
                                  <span class="badge badge-success"><i class="fa fa-check"></i></span>
                                @endif
                              </td>
                            <td>
                              @if($item->published)
                                <span class="badge badge-success"><i class="fa fa-check"></i></span>
                              @endif
                            </td>
                            <td class="td-fit">
                                <form action="{{ url('/admin/forms', ['id' => $item->id]) }}" method="post">
                                <a class="btn btn-sm btn-outline-secondary" href="{{ url('/admin/forms/'.$item->slug) }}">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a href="#" class="btn btn-sm btn-outline-primary">
                                    <i class="fa fa-cog"></i>
                                </a>
                                @method('delete')
                                @csrf
                                <button class="btn btn-sm btn-outline-danger" type="submit" id="deleteButton">
                                  <i class="fa fa-times"></i>
                                </button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
            {!! $forms->render() !!}
        </div>
    </div>
</home>
@endsection
