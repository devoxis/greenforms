@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              Add a Field
            </div>

            <div class="card-body">
              <!-- Success Message -->
              <div class="alert alert-success">
                Field Added successfully!!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form role="form" method="POST" action="/admin/fields">
                @csrf
                <!-- Name -->
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="name">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Description')}}</label>

                  <div class="col-md-6">
                    <textarea class="form-control" name="description" rows="2"></textarea>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Placeholder')}}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="placeholder">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Section')}}</label>

                  <div class="col-md-6">
                    <select id="inputState" class="form-control custom-select" name="section_id">
                      <option selected>Choose...</option>
                      @foreach($sections as $value => $key)
                        <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach
                    </select>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Field Type')}}</label>

                  <div class="col-md-6">
                    <select id="fieldtype" class="form-control custom-select" name="field_type_id">
                      <option selected>Choose...</option>
                      @foreach($fieldtypes as $type)
                        <option value="{{ $type->id }}" data-feature="{{ $type->data[1]['features'] }}">{{ $type->name }}</option>
                      @endforeach
                    </select>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Rule(s)')}}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="rules" placeholder="laravel validation rules">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <fieldset class="form-group">
                  <div class="row">
                    <legend class="col-form-label col-md-4 text-md-right pt-0">Required</legend>
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="is_required">
                      </div>
                    </div>
                  </div>
                </fieldset>
                <fieldset class="form-group">
                  <div class="row">
                    <legend class="col-form-label col-md-4 text-md-right pt-0">On Profile</legend>
                    <div class="col-md-1">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="profilesection"  value="1" name="is_profile">
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div>
                        <div id="profilesections">
                          <select class="form-control custom-select" name="profile_section_id">
                            <option selected value="0">Choose a profile section...</option>
                            @foreach($profilesections as $value => $key)
                              <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                          </select>

                          <span class="invalid-feedback">
                            Error
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </fieldset>

                <div>
                  <legend id="options" class="card-header col-form-label text-md-center col-md-12 mb-3 bg-white"><strong>Please add the field options below</strong></legend>

                  <div class="form-group row after-add-more mb-3">
                    <div class="col-md-4">&nbsp;</div>
                    <div class="input-group col-md-6 ">
                      <input type="text" class="form-control" name="options[]">
                      <div class="input-group-append change">
                        <label for="">&nbsp;</label><br/>
                        <a class="btn btn-outline btn-dark add-more"><i class="fa fa-plus"></i></a>
                      </div>
                      <span class="invalid-feedback">
                        Error
                      </span>
                    </div>
                  </div>
                </div>
                <div>
                  <legend id="structure" class="card-header col-form-label text-md-center col-md-12 mb-3 bg-white"><strong>Please add the field structure below</strong></legend>

                  <div class="form-group row after-add-structure-more mb-3">
                    <div class="col-md-4">&nbsp;</div>
                    <div class="input-group col-md-6 ">
                      <select class="custom-select custom-select" id="inputGroupSelect04" name="structure[]">
                        <option selected>Choose...</option>
                        @foreach($fields as $value => $key)
                          <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                      </select>
                      <div class="input-group-append change">
                        <label for="">&nbsp;</label><br/>
                        <a class="btn btn-outline btn-dark add-structure-more"><i class="fa fa-plus"></i></a>
                      </div>
                      <span class="invalid-feedback">
                        Error
                      </span>
                    </div>
                  </div>
                </div>


                <!-- Save Button -->
                <div class="form-group row mb-0">
                  <div class="offset-md-4 col-md-6">
                    <button type="submit" class="btn btn-primary">
                      Save
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </home>
@endsection
