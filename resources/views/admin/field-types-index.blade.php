@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">{{__('Field Types')}}
                      <span class="pull-right">
                        <a href="/admin/fieldtypes/create">
                          <button class="btn btn-sm btn-dark">
                              Add a Field Type
                          </button>
                        </a>
                      </span>
                    </div>

                    <div class="table-responsive table-hover">
                      <table class="table table-valign-middle mb-0">

                        <thead>
                          <tr>
                            <th class="th-fit"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Features</th>
                            <th>&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($response as $item)
                            <tr>
                              <th></th>
                              <td>{{ $item->name }}</td>
                              <td>
                                @foreach ($item->data as $rows)
                                  @foreach ($rows as $key => $value)
                                      <b>{{ $key }}</b>: {{ $value }} <br>
                                  @endforeach
                                @endforeach
                              </td>
                              <td class="td-fit">
                                <form action="{{ url('/admin/fieldtypes', ['id' => $item->id]) }}" method="post">
                                  <a href="#">
                                    <button class="btn btn-sm btn-outline-primary">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                  </a>
                                  @method('delete')
                                  @csrf
                                  <button class="btn btn-sm btn-outline-danger" type="submit" id="deleteButton">
                                      <i class="fa fa-times"></i>
                                  </button>
                                  </form>
                              </td>
                            </tr>
                          @endforeach

                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
            {!! $response->render() !!}
        </div>
    </div>
</home>
@endsection
