@extends('spark::layouts.app')

@section('content')
  <home :user="user" inline-template>
    <div class="container">
      <!-- Application Dashboard -->
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card card-default">
            <div class="card-header">
              Add a Form Section
            </div>

            <div class="card-body">
              <!-- Success Message -->
              <div class="alert alert-success">
                Section Added successfully!!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <form role="form" method="POST" action="/admin/sections">
                @csrf
                <!-- Name -->
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="name">

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right">{{__('Description')}}</label>

                  <div class="col-md-6">
                    <textarea class="form-control" name="description" rows="8"></textarea>

                    <span class="invalid-feedback">
                      Error
                    </span>
                  </div>
                </div>
                <fieldset class="form-group">
                  <div class="row">
                    <legend class="col-form-label col-md-4 text-md-right pt-0">Profile Section</legend>
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck1" name="is_profile" value="1">
                      </div>
                    </div>
                  </div>
                </fieldset>
                <!-- Save Button -->
                <div class="form-group row mb-0">
                  <div class="offset-md-4 col-md-6">
                    <button type="submit" class="btn btn-primary">
                      Save
                    </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</home>
@endsection
