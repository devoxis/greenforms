<?php

use App\FormHistory;
use Illuminate\Database\Seeder;

class FormHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FormHistory::class, 10)->create();
    }
}