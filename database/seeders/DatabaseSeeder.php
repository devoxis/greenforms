<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProfileSeeder::class);
        $this->call(FieldStructureSeeder::class);
        $this->call(FieldOptionSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(FieldTypeSeeder::class);
        $this->call(SectionSeeder::class);
        $this->call(FormSeeder::class);
        $this->call(FieldSeeder::class);
        $this->call(SubmissionSeeder::class);
        $this->call(StageSeeder::class);
        $this->call(StructureSeeder::class);
        $this->call(FormHistorySeeder::class);
    }
}
