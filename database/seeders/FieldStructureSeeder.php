<?php

use App\FieldStructure;
use Illuminate\Database\Seeder;

class FieldStructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FieldStructure::class, 10)->create();
    }
}