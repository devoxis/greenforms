<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('section_id')->nullable();
            $table->unsignedBigInteger('field_type_id')->nullable();
            $table->unsignedBigInteger('profile_section_id')->nullable();
            $table->string('name');
            $table->text('description');
            $table->string('placeholder')->nullable();
            $table->string('rules')->nullable();
            $table->boolean('is_required')->default(0);
            $table->boolean('is_profile')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
