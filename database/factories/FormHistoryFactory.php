<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\FormHistory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(FormHistory::class, function (Faker $faker) {
    return [
        'submission_id' => random_int(1, 10),
        'stage_id' => random_int(1, 10),
        'user_id' => random_int(0, 9223372036854775807),
        'comment' => $faker->realText(),
        'status' => BAD_DATATYPE
    ];
});
