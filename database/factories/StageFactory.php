<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Stage;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Stage::class, function (Faker $faker) {
    return [
        'form_id' => random_int(1, 10),
        'name' => $faker->name(),
        'ordering' => random_int(-128, 127),
        'is_final' => $faker->boolean(),
        'is_notify_approved' => $faker->boolean(),
        'is_notify_declined' => $faker->boolean()
    ];
});
