<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Field;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Field::class, function (Faker $faker) {
    return [
        'section_id' => random_int(1, 10),
        'field_type_id' => random_int(1, 10),
        'field_structure_id' => random_int(1, 10),
        'field_option_id' => random_int(1, 10),
        'name' => $faker->name(),
        'description' => $faker->realText(),
        'is_profile' => $faker->boolean()
    ];
});
