<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Profile;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'user_id' => random_int(0, 9223372036854775807),
        'name' => $faker->name(),
        'data' => DATATYPE_NOT_IMPLEMENTED_YET,
        'is_default' => $faker->boolean()
    ];
});
