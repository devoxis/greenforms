<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Submission;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Submission::class, function (Faker $faker) {
    return [
        'form_id' => random_int(1, 10),
        'user_id' => random_int(0, 9223372036854775807),
        'received_by' => random_int(0, 9223372036854775807),
        'slug' => $faker->slug,
        'data' => DATATYPE_NOT_IMPLEMENTED_YET,
        'is_accept' => $faker->boolean(),
        'is_received' => $faker->boolean()
    ];
});
