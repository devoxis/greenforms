<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Structure;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Structure::class, function (Faker $faker) {
    return [
        'form_id' => random_int(1, 10),
        'section_id' => random_int(0, 9223372036854775807),
        'field_id' => random_int(1, 10),
        'row' => $faker->word(),
        'size' => $faker->word(),
        'is_required' => $faker->boolean()
    ];
});
