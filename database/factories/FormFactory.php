<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Form;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Form::class, function (Faker $faker) {
    return [
        'category_id' => random_int(1, 10),
        'team_id' => random_int(0, 9223372036854775807),
        'name' => $faker->name(),
        'slug' => $faker->slug,
        'description' => $faker->realText(),
        'has_workflow' => $faker->boolean()
    ];
});
