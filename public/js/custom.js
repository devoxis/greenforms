$(document).ready(function() {
    var $options = $('#options').closest('div').hide();
    var $profilesections = $('#profilesections').closest('div').hide();
    var $structure = $('#structure').closest('div').hide();

    $("#form-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        stepsOrientation: "vertical",
        titleTemplate: '<span class="text-md">#index#. #title#</span>',
    });

    $('#fieldtype').change(function() {
        var selectedFeature = $(this).find(':selected').data("feature");

        if(selectedFeature  === 'structure') {
            $options.hide();
            $structure.show();
        } else if (selectedFeature === 'options') {
            $options.show();
            $structure.hide();
        } else {
            $options.hide();
            $structure.hide();
        }
    });

    $('#profilesection').change(function() {
        if(this.checked) {
            $profilesections.show();
        } else {
            $profilesections.hide();
        }
    });

    //add more on the fields page - form options
    $("body").on("click",".add-more",function(){
        var html = $(".after-add-more").first().clone();
          $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-outline btn-warning remove'><i class='fa fa-minus'></i></a>");
        $(".after-add-more").last().after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".after-add-more").remove();
    });
    //add more on the stages page - form options
    $("body").on("click",".add-more-stages",function(){
        var html = $(".after-add-more-stages").first().clone();
        var num = $(".after-add-more-stages").length;
        var newNum = num + 1;
        $(html).find('.name1').attr('name', "name["+newNum+"]");
        $(html).find('.select1').attr('name', "users["+newNum+"][]");
        $(html).find('.check1').attr('name', "is_notify_approved["+newNum+"]");
        $(html).find('.check2').attr('name', "is_notify_declined["+newNum+"]");
        $(html).find('.check3').attr('name', "is_payment["+newNum+"]");

        $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-outline btn-warning remove'><i class='fa fa-minus'></i></a>");
        $(".after-add-more-stages").last().after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".after-add-more-stages").remove();
    });
    //add more on the fields page - form structure
    $("body").on("click",".add-structure-more",function(){
        var html = $(".after-add-structure-more").first().clone();
          $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-outline btn-warning remove'><i class='fa fa-minus'></i></a>");
        $(".after-add-structure-more").last().after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".after-add-structure-more").remove();
    });
    // //add more fields on the new form page
    // $(".add-fields").click(function(){
    //     var html = $(".copy-fields").html();
    //     //console.log(html);
    //     $(this).parents(".after-add-fields-more").last().after(html);
    // });
    //
    // // $("#form-saving").submit(function(e){
    // //   e.preventDefault();
    // //   $(this).each(function(){
    // //       $(this).find(':select');
    // //
    // //   });
    // // });
    //
    // $("body").on("click",".add-fields1",function(){
    //     var html = $(".copy-fields").html();
    //     $(this).parents(".after-add-fields-more").last().after(html);
    // });
    //
    // $("body").on("click",".remove-fields",function(){
    //     $(this).parents(".fields-row").remove();
    // });
    // //add more sections on the new form page
    // //$("body").on("click",".add-sections-more",function(){
    // $(".add-sections-more").click(function(){
    //       var html = $(".copy-section").html();
    //
    //       $(this).parents(".after-add-sections-more").last().after(html);
    // });
    //
    // $("body").on("click",".remove-section",function(){
    //       $(this).parents(".section-row").remove();
    // });
    // add section

      $(".add-sections-more").click(function(){

            var copyDiv = $(".copy-section").find('.jumbotron-fluid').clone();
            $(copyDiv).addClass('section-div');

            var numItems = $(document).find('.section-div').length;
            console.log(numItems);
            numItems = numItems + 1;

            $(copyDiv).data('section', numItems);
            $(copyDiv).attr('data-section', numItems);
            $(copyDiv).find('.field1').attr('name', "fields1["+numItems+"][1]");
            $(copyDiv).find('.field2').attr('name', "fields2["+numItems+"][1]");
            $(copyDiv).find('.field3').attr('name', "fields3["+numItems+"][1]");

            $(copyDiv).find('.section-box').attr('name', "sections["+numItems+"]")
            //$(this).parents(".after-add-sections-more").last().after(copyDiv);
            $(this).parents(".after-add-sections-more").append(copyDiv);

      });

      // add fileds

      $("body").on("click",".add-fields",function(){

          var copyFileds = $(".copy-fields").find('.fields-row').clone();
          var rowCount = $(this).parents('.section-div').find('.after-add-fields-more .form-row').length;
          rowCount = rowCount + 1;
          var dataSection = $(this).parents('.section-div').attr('data-section');

          $(copyFileds).find('.field1').attr('name', "fields1["+dataSection+"]["+rowCount+"]")
          $(copyFileds).find('.field2').attr('name', "fields2["+dataSection+"]["+rowCount+"]")
          $(copyFileds).find('.field3').attr('name', "fields3["+dataSection+"]["+rowCount+"]")

          $(this).parents(".after-add-fields-more").append(copyFileds);
      });

      // remove fields

      $("body").on("click",".remove-fields",function(){

          $('#submit-btn').addClass('disabled');

          var parentDiv = $(this).parents(".after-add-fields-more");
          $(this).parents(".fields-row").remove();

          $(parentDiv).find('.form-row').each(function(i, obj) {

              var dataSection = $(this).parents('.section-div').attr('data-section');
              var rowNum = i + 1;
              console.log( dataSection, rowNum, $(this).find('.field1') );
              $(this).find('.field1').attr('name', "fields1["+dataSection+"]["+rowNum+"]")
              $(this).find('.field2').attr('name', "fields2["+dataSection+"]["+rowNum+"]")
              $(this).find('.field3').attr('name', "fields3["+dataSection+"]["+rowNum+"]")

          });

          $('#submit-btn').removeClass('disabled');
      });


      // remove section

      $("body").on("click",".remove-section",function(){
          $('#submit-btn').addClass('disabled');
          var parentDiv = $(this).parents(".after-add-sections-more");
          $(this).parents(".section-row").remove();

          $(parentDiv).find('.section-div').each(function(i, obj) {

              var sectionNum = i + 1;
              $(this).attr('data-section', sectionNum);
              $(this).find('.section-box').attr('name', "sections["+sectionNum+"]");
              console.log( 'section div ----- ', sectionNum );
              $(this).find('.after-add-fields-more').find('.form-row').each(function(i2, obj2) {
                  var rowNum = i2 + 1;
                  console.log('row Num ---- ', rowNum );
                  $(this).find('.field1').attr('name', "fields1["+sectionNum+"]["+rowNum+"]")
                  $(this).find('.field2').attr('name', "fields2["+sectionNum+"]["+rowNum+"]")
                  $(this).find('.field3').attr('name', "fields3["+sectionNum+"]["+rowNum+"]")
              });

          });

          $('#submit-btn').removeClass('disabled');

      });

    $('#category').on('change',function(){
      if( $(this).val()=== '0'){
        $("#NewCategory").show()
      }
      else{
        $("#NewCategory").hide()
      }
    });

    //sorting fields on profile Sections
    $( "#sortable" ).sortable({ axis: "y", containment: "#ballot", scroll: false });
    $( "#sortable" ).disableSelection();

    $('.sort-form').submit(function(){

       $('#thedata').val($( "#sortable" ).sortable("serialize"));
       return false;
    });

    $("#deleteButton").click(function(){
        if(confirm("Are you sure you want to delete this?")){
            return true;
        }
        else{
            return false;
        }
    });
});
