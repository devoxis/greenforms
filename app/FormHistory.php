<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'submission_id', 'stage_id', 'user_id', 'comment', 'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the Stage for the FormHistory.
     */
    public function stage()
    {
        return $this->belongsTo(\App\Stage::class);
    }


    /**
     * Get the Submission for the FormHistory.
     */
    public function submission()
    {
        return $this->belongsTo(\App\Submission::class);
    }

}
