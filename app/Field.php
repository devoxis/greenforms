<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'section_id', 'profile_section_id', 'field_type_id', 'name', 'description', 'placeholder', 'is_profile', 'is_required'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_profile' => 'boolean',
        'is_required' => 'boolean',
    ];

    /**
     * Get the Structures for the Field.
     */
    public function structure()
    {
        return $this->hasOne(\App\FieldStructure::class);
    }

    /**
     * Get the Options for the Field.
     */
    public function options()
    {
        return $this->hasOne(\App\FieldOption::class);
    }

    /**
     * Get the Section for the Field.
     */
    public function section()
    {
        return $this->belongsTo(\App\Section::class);
    }


    /**
     * Get the FieldType for the Field.
     */
    public function fieldtype()
    {
        return $this->belongsTo(\App\FieldType::class,'field_type_id');
    }

    /**
     * Get the Profile Section for the Field.
     */
    public function profilesection()
    {
        return $this->belongsTo(\App\Section::class,'profile_section_id');
    }

}
