<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'form_id', 'section_id', 'field_id', 'row', 'size', 'is_required'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_required' => 'boolean'
    ];

    /**
     * Get the Section for the Structure.
     */
    public function section()
    {
        return $this->belongsTo(\App\Section::class);
    }


    /**
     * Get the Field for the Structure.
     */
    public function field()
    {
        return $this->belongsTo(\App\Field::class);
    }


    /**
     * Get the Form for the Structure.
     */
    public function form()
    {
        return $this->belongsTo(\App\Form::class);
    }

}
