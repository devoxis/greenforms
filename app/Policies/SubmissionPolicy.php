<?php

namespace App\Policies;

use App\User;
use App\Submission;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubmissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any submission.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the submission.
     *
     * @param  App\User  $user
     * @param  App\Submission  $submission
     * @return bool
     */
    public function view(User $user, Submission $submission)
    {
        return false;
    }

    /**
     * Determine whether the user can create a submission.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the submission.
     *
     * @param  App\User  $user
     * @param  App\Submission  $submission
     * @return bool
     */
    public function update(User $user, Submission $submission)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the submission.
     *
     * @param  App\User  $user
     * @param  App\Submission  $submission
     * @return bool
     */
    public function delete(User $user, Submission $submission)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the submission.
     *
     * @param  App\User  $user
     * @param  App\Submission  $submission
     * @return bool
     */
    public function restore(User $user, Submission $submission)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the submission.
     *
     * @param  App\User  $user
     * @param  App\Submission  $submission
     * @return bool
     */
    public function forceDelete(User $user, Submission $submission)
    {
        return false;
    }
}
