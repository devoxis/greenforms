<?php

namespace App\Policies;

use App\User;
use App\FieldStructure;
use Illuminate\Auth\Access\HandlesAuthorization;

class FieldStructurePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any fieldStructure.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the fieldStructure.
     *
     * @param  App\User  $user
     * @param  App\FieldStructure  $fieldStructure
     * @return bool
     */
    public function view(User $user, FieldStructure $fieldStructure)
    {
        return false;
    }

    /**
     * Determine whether the user can create a fieldStructure.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the fieldStructure.
     *
     * @param  App\User  $user
     * @param  App\FieldStructure  $fieldStructure
     * @return bool
     */
    public function update(User $user, FieldStructure $fieldStructure)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the fieldStructure.
     *
     * @param  App\User  $user
     * @param  App\FieldStructure  $fieldStructure
     * @return bool
     */
    public function delete(User $user, FieldStructure $fieldStructure)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the fieldStructure.
     *
     * @param  App\User  $user
     * @param  App\FieldStructure  $fieldStructure
     * @return bool
     */
    public function restore(User $user, FieldStructure $fieldStructure)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the fieldStructure.
     *
     * @param  App\User  $user
     * @param  App\FieldStructure  $fieldStructure
     * @return bool
     */
    public function forceDelete(User $user, FieldStructure $fieldStructure)
    {
        return false;
    }
}
