<?php

namespace App\Policies;

use App\User;
use App\Field;
use Illuminate\Auth\Access\HandlesAuthorization;

class FieldPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any field.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the field.
     *
     * @param  App\User  $user
     * @param  App\Field  $field
     * @return bool
     */
    public function view(User $user, Field $field)
    {
        return false;
    }

    /**
     * Determine whether the user can create a field.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the field.
     *
     * @param  App\User  $user
     * @param  App\Field  $field
     * @return bool
     */
    public function update(User $user, Field $field)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the field.
     *
     * @param  App\User  $user
     * @param  App\Field  $field
     * @return bool
     */
    public function delete(User $user, Field $field)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the field.
     *
     * @param  App\User  $user
     * @param  App\Field  $field
     * @return bool
     */
    public function restore(User $user, Field $field)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the field.
     *
     * @param  App\User  $user
     * @param  App\Field  $field
     * @return bool
     */
    public function forceDelete(User $user, Field $field)
    {
        return false;
    }
}
