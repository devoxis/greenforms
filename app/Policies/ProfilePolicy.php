<?php

namespace App\Policies;

use App\User;
use App\Profile;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any profile.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the profile.
     *
     * @param  App\User  $user
     * @param  App\Profile  $profile
     * @return bool
     */
    public function view(User $user, Profile $profile)
    {
        return false;
    }

    /**
     * Determine whether the user can create a profile.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the profile.
     *
     * @param  App\User  $user
     * @param  App\Profile  $profile
     * @return bool
     */
    public function update(User $user, Profile $profile)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the profile.
     *
     * @param  App\User  $user
     * @param  App\Profile  $profile
     * @return bool
     */
    public function delete(User $user, Profile $profile)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the profile.
     *
     * @param  App\User  $user
     * @param  App\Profile  $profile
     * @return bool
     */
    public function restore(User $user, Profile $profile)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the profile.
     *
     * @param  App\User  $user
     * @param  App\Profile  $profile
     * @return bool
     */
    public function forceDelete(User $user, Profile $profile)
    {
        return false;
    }
}
