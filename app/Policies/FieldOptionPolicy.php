<?php

namespace App\Policies;

use App\User;
use App\FieldOption;
use Illuminate\Auth\Access\HandlesAuthorization;

class FieldOptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any fieldOption.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the fieldOption.
     *
     * @param  App\User  $user
     * @param  App\FieldOption  $fieldOption
     * @return bool
     */
    public function view(User $user, FieldOption $fieldOption)
    {
        return false;
    }

    /**
     * Determine whether the user can create a fieldOption.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the fieldOption.
     *
     * @param  App\User  $user
     * @param  App\FieldOption  $fieldOption
     * @return bool
     */
    public function update(User $user, FieldOption $fieldOption)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the fieldOption.
     *
     * @param  App\User  $user
     * @param  App\FieldOption  $fieldOption
     * @return bool
     */
    public function delete(User $user, FieldOption $fieldOption)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the fieldOption.
     *
     * @param  App\User  $user
     * @param  App\FieldOption  $fieldOption
     * @return bool
     */
    public function restore(User $user, FieldOption $fieldOption)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the fieldOption.
     *
     * @param  App\User  $user
     * @param  App\FieldOption  $fieldOption
     * @return bool
     */
    public function forceDelete(User $user, FieldOption $fieldOption)
    {
        return false;
    }
}
