<?php

namespace App\Policies;

use App\User;
use App\Form;
use Illuminate\Auth\Access\HandlesAuthorization;

class FormPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any form.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the form.
     *
     * @param  App\User  $user
     * @param  App\Form  $form
     * @return bool
     */
    public function view(User $user, Form $form)
    {
        return false;
    }

    /**
     * Determine whether the user can create a form.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the form.
     *
     * @param  App\User  $user
     * @param  App\Form  $form
     * @return bool
     */
    public function update(User $user, Form $form)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the form.
     *
     * @param  App\User  $user
     * @param  App\Form  $form
     * @return bool
     */
    public function delete(User $user, Form $form)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the form.
     *
     * @param  App\User  $user
     * @param  App\Form  $form
     * @return bool
     */
    public function restore(User $user, Form $form)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the form.
     *
     * @param  App\User  $user
     * @param  App\Form  $form
     * @return bool
     */
    public function forceDelete(User $user, Form $form)
    {
        return false;
    }
}
