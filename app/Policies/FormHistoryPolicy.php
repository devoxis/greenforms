<?php

namespace App\Policies;

use App\User;
use App\FormHistory;
use Illuminate\Auth\Access\HandlesAuthorization;

class FormHistoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any formHistory.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the formHistory.
     *
     * @param  App\User  $user
     * @param  App\FormHistory  $formHistory
     * @return bool
     */
    public function view(User $user, FormHistory $formHistory)
    {
        return false;
    }

    /**
     * Determine whether the user can create a formHistory.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the formHistory.
     *
     * @param  App\User  $user
     * @param  App\FormHistory  $formHistory
     * @return bool
     */
    public function update(User $user, FormHistory $formHistory)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the formHistory.
     *
     * @param  App\User  $user
     * @param  App\FormHistory  $formHistory
     * @return bool
     */
    public function delete(User $user, FormHistory $formHistory)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the formHistory.
     *
     * @param  App\User  $user
     * @param  App\FormHistory  $formHistory
     * @return bool
     */
    public function restore(User $user, FormHistory $formHistory)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the formHistory.
     *
     * @param  App\User  $user
     * @param  App\FormHistory  $formHistory
     * @return bool
     */
    public function forceDelete(User $user, FormHistory $formHistory)
    {
        return false;
    }
}
