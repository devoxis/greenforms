<?php

namespace App\Policies;

use App\User;
use App\Section;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any section.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the section.
     *
     * @param  App\User  $user
     * @param  App\Section  $section
     * @return bool
     */
    public function view(User $user, Section $section)
    {
        return false;
    }

    /**
     * Determine whether the user can create a section.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the section.
     *
     * @param  App\User  $user
     * @param  App\Section  $section
     * @return bool
     */
    public function update(User $user, Section $section)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the section.
     *
     * @param  App\User  $user
     * @param  App\Section  $section
     * @return bool
     */
    public function delete(User $user, Section $section)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the section.
     *
     * @param  App\User  $user
     * @param  App\Section  $section
     * @return bool
     */
    public function restore(User $user, Section $section)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the section.
     *
     * @param  App\User  $user
     * @param  App\Section  $section
     * @return bool
     */
    public function forceDelete(User $user, Section $section)
    {
        return false;
    }
}
