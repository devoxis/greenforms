<?php

namespace App\Policies;

use App\User;
use App\FieldType;
use Illuminate\Auth\Access\HandlesAuthorization;

class FieldTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any fieldType.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the fieldType.
     *
     * @param  App\User  $user
     * @param  App\FieldType  $fieldType
     * @return bool
     */
    public function view(User $user, FieldType $fieldType)
    {
        return false;
    }

    /**
     * Determine whether the user can create a fieldType.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the fieldType.
     *
     * @param  App\User  $user
     * @param  App\FieldType  $fieldType
     * @return bool
     */
    public function update(User $user, FieldType $fieldType)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the fieldType.
     *
     * @param  App\User  $user
     * @param  App\FieldType  $fieldType
     * @return bool
     */
    public function delete(User $user, FieldType $fieldType)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the fieldType.
     *
     * @param  App\User  $user
     * @param  App\FieldType  $fieldType
     * @return bool
     */
    public function restore(User $user, FieldType $fieldType)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the fieldType.
     *
     * @param  App\User  $user
     * @param  App\FieldType  $fieldType
     * @return bool
     */
    public function forceDelete(User $user, FieldType $fieldType)
    {
        return false;
    }
}
