<?php

namespace App\Policies;

use App\User;
use App\Stage;
use Illuminate\Auth\Access\HandlesAuthorization;

class StagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any stage.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the stage.
     *
     * @param  App\User  $user
     * @param  App\Stage  $stage
     * @return bool
     */
    public function view(User $user, Stage $stage)
    {
        return false;
    }

    /**
     * Determine whether the user can create a stage.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the stage.
     *
     * @param  App\User  $user
     * @param  App\Stage  $stage
     * @return bool
     */
    public function update(User $user, Stage $stage)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the stage.
     *
     * @param  App\User  $user
     * @param  App\Stage  $stage
     * @return bool
     */
    public function delete(User $user, Stage $stage)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the stage.
     *
     * @param  App\User  $user
     * @param  App\Stage  $stage
     * @return bool
     */
    public function restore(User $user, Stage $stage)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the stage.
     *
     * @param  App\User  $user
     * @param  App\Stage  $stage
     * @return bool
     */
    public function forceDelete(User $user, Stage $stage)
    {
        return false;
    }
}
