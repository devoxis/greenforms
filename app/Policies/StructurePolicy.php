<?php

namespace App\Policies;

use App\User;
use App\Structure;
use Illuminate\Auth\Access\HandlesAuthorization;

class StructurePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any structure.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the structure.
     *
     * @param  App\User  $user
     * @param  App\Structure  $structure
     * @return bool
     */
    public function view(User $user, Structure $structure)
    {
        return false;
    }

    /**
     * Determine whether the user can create a structure.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the structure.
     *
     * @param  App\User  $user
     * @param  App\Structure  $structure
     * @return bool
     */
    public function update(User $user, Structure $structure)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the structure.
     *
     * @param  App\User  $user
     * @param  App\Structure  $structure
     * @return bool
     */
    public function delete(User $user, Structure $structure)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the structure.
     *
     * @param  App\User  $user
     * @param  App\Structure  $structure
     * @return bool
     */
    public function restore(User $user, Structure $structure)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the structure.
     *
     * @param  App\User  $user
     * @param  App\Structure  $structure
     * @return bool
     */
    public function forceDelete(User $user, Structure $structure)
    {
        return false;
    }
}
