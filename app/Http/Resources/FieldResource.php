<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FieldResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'section_id' => $this->section_id,
            'field_type_id' => $this->field_type_id,
            'profile_section_id' => $this->profile_section_id,
            'name' => $this->name,
            'description' => $this->description,
            'is_required' => $this->is_required,
            'is_profile' => $this->is_profile,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'section' => new SectionResource($this->whenLoaded('section')),
            'profilesection' => new SectionResource($this->whenLoaded('profilesection')),
            'field_type' => new FieldTypeResource($this->whenLoaded('field_type'))
        ];
    }
}
