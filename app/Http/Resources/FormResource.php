<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FormResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'team_id' => $this->team_id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'has_workflow' => $this->has_workflow,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'structures' => new StructureCollection($this->whenLoaded('structures')),
            'stages' => new StageCollection($this->whenLoaded('stages')),
            'submissions' => new SubmissionCollection($this->whenLoaded('submissions')),
            'category' => new CategoryResource($this->whenLoaded('category'))
        ];
    }
}
