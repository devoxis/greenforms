<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubmissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'form_id' => $this->form_id,
            'user_id' => $this->user_id,
            'received_by' => $this->received_by,
            'slug' => $this->slug,
            'data' => $this->data,
            'is_accept' => $this->is_accept,
            'is_received' => $this->is_received,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'form_histories' => new FormHistoryCollection($this->whenLoaded('form_histories')),
            'form' => new FormResource($this->whenLoaded('form'))
        ];
    }
}
