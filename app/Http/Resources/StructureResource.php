<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StructureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'form_id' => $this->form_id,
            'section_id' => $this->section_id,
            'field_id' => $this->field_id,
            'row' => $this->row,
            'size' => $this->size,
            'is_required' => $this->is_required,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'section' => new SectionResource($this->whenLoaded('section')),
            'field' => new FieldResource($this->whenLoaded('field')),
            'form' => new FormResource($this->whenLoaded('form'))
        ];
    }
}
