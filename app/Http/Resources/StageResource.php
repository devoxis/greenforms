<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'form_id' => $this->form_id,
            'name' => $this->name,
            'ordering' => $this->ordering,
            'is_final' => $this->is_final,
            'is_notify_approved' => $this->is_notify_approved,
            'is_notify_declined' => $this->is_notify_declined,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'stage_users' => new StageUserCollection($this->whenLoaded('stage_users')),
            'form_histories' => new FormHistoryCollection($this->whenLoaded('form_histories')),
            'form' => new FormResource($this->whenLoaded('form'))
        ];
    }
}
