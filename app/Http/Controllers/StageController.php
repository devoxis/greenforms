<?php

namespace App\Http\Controllers;

use App\Form;
use App\Team;
use App\Stage;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class StageController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create($slug)
    {
        $form = Form::whereSlug($slug)->first();
        $users = Team::find($form->team_id)->users;
        return view('admin.workflow-form', compact('form','users'));
    }

    public function store(Request $request)
    {
        //dd($request);
        for($i=1;$i < count($request->name)+1;$i++)
        {
          $stage = new Stage;
          $stage->name = $request->name[$i];
          $stage->form_id = $request->form_id;
          $stage->users = json_encode($request->users[$i]);
          $stage->ordering = $i;
          if(isset($request->is_notify_approved[$i])){
            $stage->is_notify_approved = $request->is_notify_approved[$i];
          }
          if(isset($request->is_notify_declined[$i]))
          {
            $stage->is_notify_declined = $request->is_notify_declined[$i];
          }
          if(isset($request->is_payment[$i]))
          {
            $stage->is_payment = $request->is_payment[$i];
          }
          if($i == count($request->name))
          {
            $stage->is_final = 1;
          }else{
            $stage->is_final = 0;
          }
          $stage->save();
        }
        return redirect('/admin/forms/');

    }
}
