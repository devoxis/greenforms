<?php

namespace App\Http\Controllers;

use App\Form;
use App\Team;
use App\Section;
use App\Category;
use App\Field;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class FormController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $forms = Form::paginate(10);
        return view('admin.forms-index', compact('forms'));
    }

    public function create()
    {
      $sections = Section::where('is_profile',0)->pluck('id','name');
      $teams = Team::orderBy('name')->get()->pluck('id','name');
      $categories = Category::orderBy('name')->get();
      $fields = Field::pluck('id','name');
      return view('admin.forms-form', compact('sections','teams','categories','fields'));
    }

    public function view($slug)
    {
        $form = Form::whereSlug($slug)->first();
        return view('user.forms-view', compact('form'));
    }
}
