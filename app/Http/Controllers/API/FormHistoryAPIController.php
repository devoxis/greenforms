<?php

namespace App\Http\Controllers\API;

use App\FormHistory;
use App\Http\Resources\FormHistoryCollection;
use App\Http\Resources\FormHistoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormHistoryAPIController extends Controller
{
    public function index()
    {
        return new FormHistoryCollection(FormHistory::paginate());
    }
 
    public function show(FormHistory $formHistory)
    {
        return new FormHistoryResource($formHistory->load(['stage', 'submission']));
    }

    public function store(Request $request)
    {
        return new FormHistoryResource(FormHistory::create($request->all()));
    }

    public function update(Request $request, FormHistory $formHistory)
    {
        $formHistory->update($request->all());

        return new FormHistoryResource($formHistory);
    }

    public function destroy(Request $request, FormHistory $formHistory)
    {
        $formHistory->delete();

        return response()->noContent();
    }
}
