<?php

namespace App\Http\Controllers\API;

use App\Section;
use App\Http\Resources\SectionCollection;
use App\Http\Resources\SectionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectionAPIController extends Controller
{
    public function index(Request $request)
    {
        //return new SectionCollection(Section::paginate());
        if ($request->wantsJson()) {
          // return JSON-formatted response
          $response = new SectionCollection(Section::paginate());
          return $response;
        } else {
          // return HTML response
          $response = Section::paginate();

          return view('admin.sections-index', compact('response'));
        }
    }

    public function show(Section $section)
    {
        return new SectionResource($section->load(['fields', 'structures']));
    }

    public function store(Request $request)
    {

        $response = new SectionResource(Section::create($request->all()));
        if ($request->wantsJson()) {
          // return JSON-formatted response
          return $response;
        } else {
          // return HTML response
          return redirect('/admin/sections');
        }
    }

    public function update(Request $request, Section $section)
    {
        $section->update($request->all());

        return new SectionResource($section);
    }

    public function destroy(Request $request, Section $section)
    {
      $section->delete();

      if ($request->wantsJson()) {
        return response()->noContent();
      } else {
        return redirect('/admin/sections');
      }
    }
}
