<?php

namespace App\Http\Controllers\API;

use App\FieldStructure;
use App\Http\Resources\FieldStructureCollection;
use App\Http\Resources\FieldStructureResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FieldStructureAPIController extends Controller
{
    public function index()
    {
        return new FieldStructureCollection(FieldStructure::paginate());
    }
 
    public function show(FieldStructure $fieldStructure)
    {
        return new FieldStructureResource($fieldStructure->load([]));
    }

    public function store(Request $request)
    {
        return new FieldStructureResource(FieldStructure::create($request->all()));
    }

    public function update(Request $request, FieldStructure $fieldStructure)
    {
        $fieldStructure->update($request->all());

        return new FieldStructureResource($fieldStructure);
    }

    public function destroy(Request $request, FieldStructure $fieldStructure)
    {
        $fieldStructure->delete();

        return response()->noContent();
    }
}
