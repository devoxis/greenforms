<?php

namespace App\Http\Controllers\API;

use App\Stage;
use App\Http\Resources\StageCollection;
use App\Http\Resources\StageResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StageAPIController extends Controller
{
    public function index()
    {
        return new StageCollection(Stage::paginate());
    }
 
    public function show(Stage $stage)
    {
        return new StageResource($stage->load(['stageUsers', 'formHistories', 'form']));
    }

    public function store(Request $request)
    {
        return new StageResource(Stage::create($request->all()));
    }

    public function update(Request $request, Stage $stage)
    {
        $stage->update($request->all());

        return new StageResource($stage);
    }

    public function destroy(Request $request, Stage $stage)
    {
        $stage->delete();

        return response()->noContent();
    }
}
