<?php

namespace App\Http\Controllers\API;

use App\Team;
use App\Form;
use App\Category;
use App\Http\Resources\FormCollection;
use App\Http\Resources\FormResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormAPIController extends Controller
{
    public function index()
    {
        return new FormCollection(Form::paginate());
    }

    public function show(Form $form)
    {
        return new FormResource($form->load(['structures', 'stages', 'submissions', 'category']));
    }

    public function store(Request $request)
    {
        //dd($request);
        if ($request->wantsJson()) {
          // return JSON-formatted response
          return new FormResource(Form::create($request->all()));
        } else {
          // return HTML response
          $form = new Form;

          $form->name = $request->name;
          $form->team_id = $request->team_id;
          if($request->category_id == 0){
            $category = new Category;
            $category->team_id = $request->team_id;
            $category->name = $request->category_name;
            $category->save();
            $form->category_id = $category->id;
          }else{
            $form->category_id = $request->category_id;
          }
          $form->description = $request->description;
          $form->header = $request->header;
          $form->footer = $request->footer;
          if($form->has_delivery == 1){
            $form->has_delivery = 1;
          }else{
            $form->has_delivery = 0;
          }
          //save form structures
          $structure = array();
          foreach($request->sections as $key => $value)
          {
             for($i=1;$i < count($request->fields1[$key])+1; $i++)
             {
                $structure[$value][$i] = array($request->fields1[$key][$i],$request->fields2[$key][$i],$request->fields3[$key][$i]);
             }
          }
          $form->structure = json_encode($structure);

          $form->published = 0;
          $form->save();

          return redirect('/admin/forms/workflow/'.$form->slug);
        }
    }

    public function update(Request $request, Form $form)
    {
        $form->update($request->all());

        return new FormResource($form);
    }

    public function destroy(Request $request, Form $form)
    {
        $form->delete();

        return response()->noContent();
    }
}
