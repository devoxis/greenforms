<?php

namespace App\Http\Controllers\API;

use App\Submission;
use App\Http\Resources\SubmissionCollection;
use App\Http\Resources\SubmissionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubmissionAPIController extends Controller
{
    public function index()
    {
        return new SubmissionCollection(Submission::paginate());
    }
 
    public function show(Submission $submission)
    {
        return new SubmissionResource($submission->load(['formHistories', 'form']));
    }

    public function store(Request $request)
    {
        return new SubmissionResource(Submission::create($request->all()));
    }

    public function update(Request $request, Submission $submission)
    {
        $submission->update($request->all());

        return new SubmissionResource($submission);
    }

    public function destroy(Request $request, Submission $submission)
    {
        $submission->delete();

        return response()->noContent();
    }
}
