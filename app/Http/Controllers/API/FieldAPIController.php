<?php

namespace App\Http\Controllers\API;

use App\Field;
use App\FieldType;
use App\FieldOption;
use App\FieldStructure;
use App\Http\Resources\FieldCollection;
use App\Http\Resources\FieldResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FieldAPIController extends Controller
{
    public function index(Request $request)
    {
        //return new SectionCollection(Section::paginate());
        if ($request->wantsJson()) {
          // return JSON-formatted response
          $response = new FieldCollection(Field::paginate());
          return $response;
        } else {
          // return HTML response
          $response = Field::with(['options', 'structure'])->paginate();

          return view('admin.fields-index', compact('response'));
        }
    }

    public function show(Field $field)
    {
        return new FieldResource($field->load(['structures', 'section', 'fieldType']));
    }

    public function store(Request $request)
    {

        $is_required = isset($request->is_required) ? 1 : 0;
        $is_profile = isset($request->is_profile) ? 1 : 0;
        $profilesection = isset($request->profile_section_id) ? $request->profile_section_id : 0;

        $model = Field::updateOrCreate([
                        'section_id' => $request->section_id,
                        'field_type_id' => $request->field_type_id,
                        'name' => $request->name,
                        'description' => $request->description,
                        'placeholder' => $request->placeholder,
                        'is_required' => $is_required,
                        'is_profile' => $is_profile,
                        'profile_section_id' => $profilesection,
                      ]);

        if($model->fieldtype->data[1]['features'] === "structure")
        {
          $structure = FieldStructure::updateOrCreate([
            'field_id' => $model->id,
            'data' => $request->structure
          ]);

        }elseif ($model->fieldtype->data[1]['features'] === "options") {
          $options = FieldOption::updateOrCreate([
            'field_id' => $model->id,
            'data' => $request->options
          ]);
        }

        if ($request->wantsJson()) {
          // return JSON-formatted response
          return new FieldResource($model);
        } else {
          // return HTML response
          return redirect('/admin/fields');
        }
    }

    public function update(Request $request, Field $field)
    {
        $field->update($request->all());

        return new FieldResource($field);
    }

    public function destroy(Request $request, Field $field)
    {
        if($field->options)
        {
          $field->options->delete();
        }
        if($field->structure)
        {
          $field->structure->delete();
        }
        $field->delete();

        if ($request->wantsJson()) {
          return response()->noContent();
        } else {
          return redirect('/admin/fields');
        }
    }
}
