<?php

namespace App\Http\Controllers\API;

use App\Structure;
use App\Http\Resources\StructureCollection;
use App\Http\Resources\StructureResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StructureAPIController extends Controller
{
    public function index()
    {
        return new StructureCollection(Structure::paginate());
    }
 
    public function show(Structure $structure)
    {
        return new StructureResource($structure->load(['section', 'field', 'form']));
    }

    public function store(Request $request)
    {
        return new StructureResource(Structure::create($request->all()));
    }

    public function update(Request $request, Structure $structure)
    {
        $structure->update($request->all());

        return new StructureResource($structure);
    }

    public function destroy(Request $request, Structure $structure)
    {
        $structure->delete();

        return response()->noContent();
    }
}
