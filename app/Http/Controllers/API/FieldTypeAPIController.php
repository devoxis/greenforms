<?php

namespace App\Http\Controllers\API;

use App\FieldType;
use App\Http\Resources\FieldTypeCollection;
use App\Http\Resources\FieldTypeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FieldTypeAPIController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
          // return JSON-formatted response
          $response = new FieldTypeCollection(FieldType::paginate());
          return $response;
        } else {
          // return HTML response
          $response = FieldType::paginate();

          return view('admin.field-types-index', compact('response'));
        }
    }

    public function show(FieldType $fieldtype)
    {
        return new FieldTypeResource($fieldtype->load(['fields']));
    }

    public function store(Request $request)
    {
        $response = new FieldTypeResource(FieldType::create($request->all()));
        if ($request->wantsJson()) {
          // return JSON-formatted response
          return $response;
        } else {
          // return HTML response
          return redirect('/admin/fieldtypes');
        }
    }

    public function update(Request $request, FieldType $fieldtype)
    {
        $fieldtype->update($request->all());

        return new FieldTypeResource($fieldtype);
    }

    public function destroy(Request $request, FieldType $fieldtype)
    {
        $fieldtype->delete();

        if ($request->wantsJson()) {
          return response()->noContent();
        } else {
          return redirect('/admin/fieldtypes');
        }

    }
}
