<?php

namespace App\Http\Controllers\API;

use App\FieldOption;
use App\Http\Resources\FieldOptionCollection;
use App\Http\Resources\FieldOptionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FieldOptionAPIController extends Controller
{
    public function index()
    {
        return new FieldOptionCollection(FieldOption::paginate());
    }
 
    public function show(FieldOption $fieldOption)
    {
        return new FieldOptionResource($fieldOption->load([]));
    }

    public function store(Request $request)
    {
        return new FieldOptionResource(FieldOption::create($request->all()));
    }

    public function update(Request $request, FieldOption $fieldOption)
    {
        $fieldOption->update($request->all());

        return new FieldOptionResource($fieldOption);
    }

    public function destroy(Request $request, FieldOption $fieldOption)
    {
        $fieldOption->delete();

        return response()->noContent();
    }
}
