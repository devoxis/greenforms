<?php

namespace App\Http\Controllers;

use App\Field;
use App\Section;
use App\FieldType;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class FieldController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('admin.fields-index');
    }

    public function create()
    {
        $sections = Section::where('is_profile',0)->pluck('id','name');
        $profilesections = Section::where('is_profile',1)->pluck('id','name');
        $fieldtypes = FieldType::all();
        $fields = Field::pluck('id','name');

        return view('admin.fields-form', compact('sections','fieldtypes','profilesections','fields'));
    }
}
