<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'category_id', 'team_id', 'name', 'slug', 'description', 'has_workflow', 'published'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'has_workflow' => 'boolean',
        'published' => 'boolean'
    ];

    /**
     * Get the Structures for the Form.
     */
    public function structures()
    {
        return $this->hasMany(\App\Structure::class);
    }


    /**
     * Get the Stages for the Form.
     */
    public function stages()
    {
        return $this->hasMany(\App\Stage::class);
    }


    /**
     * Get the Submissions for the Form.
     */
    public function submissions()
    {
        return $this->hasMany(\App\Submission::class);
    }


    /**
     * Get the Category for the Form.
     */
    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    /**
     * Get the Team for the Form.
     */
    public function team()
    {
        return $this->belongsTo(\App\Team::class);
    }
}
