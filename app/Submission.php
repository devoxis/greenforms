<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'form_id', 'user_id', 'received_by', 'slug', 'data', 'is_accept', 'is_received'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'object',
        'is_accept' => 'boolean',
        'is_received' => 'boolean'
    ];

    /**
     * Get the FormHistories for the Submission.
     */
    public function formHistories()
    {
        return $this->hasMany(\App\FormHistory::class);
    }


    /**
     * Get the Form for the Submission.
     */
    public function form()
    {
        return $this->belongsTo(\App\Form::class);
    }

}
