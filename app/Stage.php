<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'form_id', 'name', 'ordering', 'is_final', 'is_notify_approved', 'is_notify_declined'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_final' => 'boolean',
        'is_notify_approved' => 'boolean',
        'is_notify_declined' => 'boolean'
    ];

    /**
     * Get the Stage_Users for the Stage.
     */
    public function stageUsers()
    {
        return $this->hasMany(\App\Stage_User::class);
    }


    /**
     * Get the FormHistories for the Stage.
     */
    public function formHistories()
    {
        return $this->hasMany(\App\FormHistory::class);
    }


    /**
     * Get the Form for the Stage.
     */
    public function form()
    {
        return $this->belongsTo(\App\Form::class);
    }

}
