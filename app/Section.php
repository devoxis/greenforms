<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name','description','data','is_profile'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_profile' => 'boolean',
        'data' => 'array'
    ];

    /**
     * Get the Fields for the Section.
     */
    public function fields()
    {
        return $this->hasMany(\App\Field::class);
    }


    /**
     * Get the Structures for the Section.
     */
    public function structures()
    {
        return $this->hasMany(\App\Structure::class);
    }

}
