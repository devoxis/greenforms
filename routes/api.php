<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Snippet for a quick route reference
*/
Route::get('/', function (Router $router) {
    return collect($router->getRoutes()->getRoutesByMethod()["GET"])->map(function($value, $key) {
        return url($key);
    })->values();   
});

Route::apiResource('profiles', '\App\Http\Controllers\API\ProfileAPIController');

Route::apiResource('sections', '\App\Http\Controllers\API\SectionAPIController');

Route::apiResource('fieldTypes', '\App\Http\Controllers\API\FieldTypeAPIController');

Route::apiResource('fieldStructures', '\App\Http\Controllers\API\FieldStructureAPIController');

Route::apiResource('fieldOptions', '\App\Http\Controllers\API\FieldOptionAPIController');

Route::apiResource('fields', '\App\Http\Controllers\API\FieldAPIController');

Route::apiResource('categories', '\App\Http\Controllers\API\CategoryAPIController');

Route::apiResource('forms', '\App\Http\Controllers\API\FormAPIController');

Route::apiResource('structures', '\App\Http\Controllers\API\StructureAPIController');

Route::apiResource('stages', '\App\Http\Controllers\API\StageAPIController');

Route::apiResource('submissions', '\App\Http\Controllers\API\SubmissionAPIController');

Route::apiResource('formHistories', '\App\Http\Controllers\API\FormHistoryAPIController');