<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');

// Admin Section

//Sections
Route::get('/admin/sections', 'API\SectionAPIController@index');
Route::post('/admin/sections', 'API\SectionAPIController@store');
Route::get('/admin/sections/create', 'SectionController@create');
Route::get('/admin/sections/view', 'SectionController@view');
Route::delete('/admin/sections/{section}', 'API\SectionAPIController@destroy');
//Field Types
Route::get('/admin/fieldtypes', 'API\FieldTypeAPIController@index');
Route::post('/admin/fieldtypes', 'API\FieldTypeAPIController@store');
Route::delete('/admin/fieldtypes/{fieldtype}', 'API\FieldTypeAPIController@destroy');
Route::get('/admin/fieldtypes/create', 'FieldTypeController@create');
//Fields
Route::get('/admin/fields', 'API\FieldAPIController@index');
Route::post('/admin/fields', 'API\FieldAPIController@store');
Route::get('/admin/fields/create', 'FieldController@create');
Route::delete('/admin/fields/{field}', 'API\FieldAPIController@destroy');
//Forms
Route::get('/admin/forms', 'FormController@index');
Route::post('/admin/forms', 'API\FormAPIController@store');
Route::get('/admin/forms/create', 'FormController@create');
Route::get('/admin/forms/{slug}', 'FormController@view');
Route::get('/admin/forms/workflow/{slug}', 'StageController@create');
Route::post('/admin/forms/workflow', 'StageController@store');

// User Section

//Profiles
Route::get('/profiles', 'ProfileController@index');
Route::get('/profiles/create', 'ProfileController@create');

//Submissions
Route::get('/submissions', 'SubmissionController@index');
Route::get('/submissions/create', 'SubmissionController@create');
Route::get('/submissions/view', 'SubmissionController@view');

//Forms
Route::get('/forms/view', 'FormController@view');
